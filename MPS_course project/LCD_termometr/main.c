#define F_CPU 1600000
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <util/delay.h>
#include "n3310.h"		//���������� ��� LCD
#include "dht11.h"		//���������� ��� �������

char buffer[8];		//����� ��� ������ �� �������

int main(void)
{
	LcdInit(); //������������� ������� 
	
	int8_t temperature = 0;
	int8_t humidity = 0;
	
    while (1) 
    {
		humidity = dht11_gethumidity();			//������� ��������� ���������
		temperature = dht11_gettemperature();	//������� ��������� �����������
		 
		LcdGotoXYFont(1,2); //���������� ������ � ������� ����������
		sprintf(buffer, "t = %02d", temperature); //��������� ����� � ����������� � ���� ������
		LcdStr(FONT_2X,buffer); //������� �� �������
		
		LcdGotoXYFont(1,4); //���������� ������ � ������� ����������
		sprintf(buffer, "v = %02d", humidity);  //��������� ����� � ��������� � ���� ������
		LcdStr(FONT_2X,buffer);  //������� �� �������
		
		LcdUpdate();	//��������� �������
    }
}

