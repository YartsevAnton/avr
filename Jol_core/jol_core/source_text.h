#define main_menu_1 " write 0"
#define main_menu_2 " write 1"
#define main_menu_3 " led control"
#define main_menu_4 " options"

#define options_menu_1 " led setup"
#define options_menu_2 " -----------"
#define options_menu_3 " information"
#define options_menu_4 " go back"

#define led_setup_1 " off"
#define led_setup_2 " slow"
#define led_setup_3 " fast"
#define led_setup_4 " on"

#define info_menu_1 " pin info"
#define info_menu_2 " --------"
#define info_menu_3 " about"
#define info_menu_4 " go back"