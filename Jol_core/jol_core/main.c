//#define F_CPU 1600000
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <avr/interrupt.h>
#include <avr/eeprom.h>
#include <util/delay.h>
#include <stdlib.h>
#include <stdio.h>
#include "menu.h"
#include "n3310.h"
int flag=0;
//main timer
ISR(TIMER1_OVF_vect)
{
		//check_menu_button();
		//_delay_ms(3000);
		if (flag==0)
		{
			 PORTC |= _BV(PC3);
			 flag=1;
		}
		else
		{
		 PORTC &= ~_BV(PC3);
		 flag=0;
		}
}

int main(void)
{
	//-------------set pots(0-input | 1-output)----------------------
	DDRD |= (0<<BTN_DOWN); //set button down
	DDRD |= (0<<BTN_UP); //set button up
	DDRC |= (0<<BTN_OK); //set button ok 
	
	DDRC |= (1<<PC4); //set PC4 as output
	DDRC |= (1<<PC3); //set PC3 as output (for LCD led)
	//---------------------------------------------------------------
	
	//---------------------turn ports--------------------------------
	PORTD |= (1<<BTN_DOWN); //Button DOWN
	PORTD |= (1<<BTN_UP); //Button UP
	PORTC |= (1<<BTN_OK); //Button_OK
	
	PORTC |= (0<<PC4);//turn off PC4
	PORTC |= (0<<PC3);//turn off PC3 (for LCD led)
	//---------------------------------------------------------------
	
	//TCCR1B = (0<<CS12)|(1<<CS11)|(1<<CS10); 
	//001 - 1    010 - 8    011 - 64    100 - 256    101 - 1024
	TIMSK |= (1<<TOIE1); 
	TCNT1 = 0; 
	TCNT2 = 0; 	
	sei(); 
		
	LcdInit();
	LcdContrast(0x40);
	print_menu(0);  
	
	while(1)
	{
		check_menu_button();
		_delay_ms(1700);
	}
}