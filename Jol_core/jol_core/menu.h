//#define F_CPU 1600000
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <util/delay.h>
#include "n3310.h"
#include "source_text.h"
#include "source_func.h"

#define BTN_UP   PD4
#define BTN_OK     PC5
#define BTN_DOWN    PD5

//---------menu----------------------------
int menu_select=0; //0-main menu, 1-options, 2-info
//-----------------------------------------

unsigned int var_2 EEMEM; //���������� var_2
unsigned int a_1;

int position=0;
char buffer[8];

int xx=0;

//----------------------------------------------------------------------
void check_menu_button(void)
{
	a_1 = eeprom_read_byte(&var_2); //read eeprom
	lcd_led_setup(a_1);
	
	if(bit_is_clear(PIND,BTN_DOWN)) //Button down
	{
		position++;
		if (position==4)
		{
			position=0;
		}
		print_menu(position);
	}
	
	if(bit_is_clear(PIND,BTN_UP)) //Button up
	{
		position--;
		if (position==-1)
		{
			position=3;
		}
		print_menu(position);
	}

	if(bit_is_clear(PINC,BTN_OK)) //Button OK
	{
		//main menu --> write 0
		if (position==0 && menu_select==0)
		{
			eeprom_write_byte(&var_2, 0); //write to var_eeprom (eeprom) 
			//eeprom_write_word(&var_eeprom, 600);
			LcdUpdate();
		}
		
		//main menu --> write 1
		if (position==1 && menu_select==0)
		{
			eeprom_write_byte(&var_2, 1); //write to var_eeprom (eeprom)
			LcdUpdate();
		}
		
		//main menu --> test func
		if (position==2 && menu_select==0)
		{
			lcd_led_setup(0); //test func.
		}
		
		//main --> options --> information --> pin info
		if (position==0 && menu_select==2)
		{
			LcdClear();
			LcdGotoXYFont(0,0);
			LcdFStr(FONT_1X,(unsigned char*)PSTR("1pin => VCC"));
			LcdGotoXYFont(0,1);
			LcdFStr(FONT_1X,(unsigned char*)PSTR("2pin => GND"));
			LcdGotoXYFont(0,2);
			LcdFStr(FONT_1X,(unsigned char*)PSTR("3pin => SCK"));
			LcdGotoXYFont(0,3);
			LcdFStr(FONT_1X,(unsigned char*)PSTR("4pin => MISO"));
			LcdGotoXYFont(0,4);
			LcdFStr(FONT_1X,(unsigned char*)PSTR("5pin => MOSI"));
			LcdGotoXYFont(0,5);
			LcdFStr(FONT_1X,(unsigned char*)PSTR("6p>Res|7p>PC4"));
		}
		
		//main --> options --> information --> about
		if (position==2 && menu_select==2)
		{
			about();
		}
			
		//3 select, options menu
		if (position==2 && menu_select==1) 
		{
			menu_select=2;
			position=0;
			print_menu(0);
		}
		
		//0 select, options menu
		if (position==0 && menu_select==1)
		{
			menu_select=3;
			position=0;
			print_menu(0);
		}
		
		if (menu_select==3)
		{
			switch (position) //options
			{
				case 0:
				lcd_led_setup(0);
				break;
				
				case 1:
				lcd_led_setup(1);
				break;
				
				case 2: 
				lcd_led_setup(2);
				break;
				
				case 3:
				lcd_led_setup(3);
				break;
			}
		}
		
		if (position==1 && menu_select==1)
		{
			
		}
		
		//go back
		if (position==3)
		{
			switch (menu_select) //options
			{
				case 0: menu_select=1;
				position=0;
				print_menu(0);
				break;
				
				case 1: menu_select=0;
				position=0;
				print_menu(0);
				break;
				
				case 2: menu_select=1;
				position=0;
				print_menu(0);
				break;
			}
		}
	}
	LcdUpdate();
}
//-------------------------------------------------------------------

//---------------------Print menu to LCD-----------------------------
void print_menu (int pos)
{
	LcdClear(); 
	
	//print main
	if (menu_select==0)
	{
		LcdGotoXYFont(0,0);
		LcdFStr(FONT_1X,(unsigned char*)PSTR(main_menu_1));  //1
		LcdGotoXYFont(0,1);
		LcdFStr(FONT_1X,(unsigned char*)PSTR(main_menu_2));  //2
		LcdGotoXYFont(0,2);
		LcdFStr(FONT_1X,(unsigned char*)PSTR(main_menu_3));  //3
		LcdGotoXYFont(0,3);
		LcdFStr(FONT_1X,(unsigned char*)PSTR(main_menu_4));  //4
	}
	
	//print main --> options 
	if (menu_select==1)
	{
		LcdGotoXYFont(0,0);
		LcdFStr(FONT_1X,(unsigned char*)PSTR(options_menu_1));  //1
		LcdGotoXYFont(0,1);
		LcdFStr(FONT_1X,(unsigned char*)PSTR(options_menu_2));  //2
		LcdGotoXYFont(0,2);
		LcdFStr(FONT_1X,(unsigned char*)PSTR(options_menu_3));  //3
		LcdGotoXYFont(0,3);
		LcdFStr(FONT_1X,(unsigned char*)PSTR(options_menu_4));  //4
	}
	
	//print main --> options --> informations
	if (menu_select==2)
	{
		LcdGotoXYFont(0,0);
		LcdFStr(FONT_1X,(unsigned char*)PSTR(info_menu_1));  //1
		LcdGotoXYFont(0,1);
		LcdFStr(FONT_1X,(unsigned char*)PSTR(info_menu_2));  //2
		LcdGotoXYFont(0,2);
		LcdFStr(FONT_1X,(unsigned char*)PSTR(info_menu_3));  //3
		LcdGotoXYFont(0,3);
		LcdFStr(FONT_1X,(unsigned char*)PSTR(info_menu_4));  //4
	}
	
		//print main --> options --> led setup
		if (menu_select==3)
		{
			a_1 = eeprom_read_byte(&var_2); //read eeprom
			if (a_1==0)
			{
				sprintf(buffer, "> %s", led_setup_1);
				LcdGotoXYFont(0,0);
				LcdStr(FONT_1X,buffer);
				LcdUpdate();
			}
			else
			{
				LcdGotoXYFont(0,0);
				LcdFStr(FONT_1X,(unsigned char*)PSTR(led_setup_1));  //1
				LcdUpdate();
			}
			
			if (a_1==1)
			{
				sprintf(buffer, "> %s", led_setup_2);
				LcdGotoXYFont(0,1);
				LcdStr(FONT_1X,buffer);
				LcdUpdate();
			}
			else
			{
				LcdGotoXYFont(0,1);
				LcdFStr(FONT_1X,(unsigned char*)PSTR(led_setup_2));  //2
			LcdUpdate();
			}
			if (a_1==2)
			{
				sprintf(buffer, "> %s", led_setup_3);
				LcdGotoXYFont(0,2);
				LcdStr(FONT_1X,buffer);
				LcdUpdate();
			}
			else
			{
				LcdGotoXYFont(0,2);
				LcdFStr(FONT_1X,(unsigned char*)PSTR(led_setup_3));  //3
				LcdUpdate();
			}
			if (a_1==3)
			{
				sprintf(buffer, "> %s", led_setup_4);
				LcdGotoXYFont(0,3);
				LcdStr(FONT_1X,buffer);
				LcdUpdate();
			}
			else
			{
				LcdGotoXYFont(0,3);
				LcdFStr(FONT_1X,(unsigned char*)PSTR(led_setup_4));  //4
				LcdUpdate();
			}
		}
	
	
	//--------string allow on bottom-------------------
	//LcdGotoXYFont(0,4);
	//a_1 = eeprom_read_byte(&var_2); //read eeprom
	//lcd_led_setup(a_1);
	//sprintf(buffer, "eeprom = %d", a_1);
	//LcdStr(FONT_1X,buffer);
	//-------------------------------------------------
	
	//LcdRect (0, 0, 82, 32, 1);   

	switch (pos)
	{
		//case 0: LcdRect (0,0,83,8,1);
		case 0: LcdSingleBar (1,8,8,81,2);
		break;
		
		case 1: LcdSingleBar (1,16,9,81,2);
		break;
		
		case 2: LcdSingleBar (1,24,9,81,2);
		break;
		
		case 3: LcdSingleBar (1,31,8,81,2);
		break;
	}
	//LcdRect (0, 0, 82, 32, 1);   
	LcdUpdate();  //anti-blink
}
//---------------------------------------------------------------------


void lcd_led_setup(int i)
{
	/*
	LcdClear();
	int x=0, y=48, xx=2, yy=rand() % 48;
	for (int i = 0; i < 45; i++)
	{
		LcdLine(x, y, xx, yy, 1 );
		x=xx;
		y=yy;
		xx=xx+2;
		yy=rand() % 48;
		LcdUpdate();
	}
	*/
	if (i==0) //turn o�f led (timer)
	{
		TIMSK &= (0<<TOIE1); 
		PORTC &= ~_BV(PC3);
		eeprom_write_byte(&var_2, 0); //0
	}
	if (i==1) //slow led
	{
		TIMSK |= (1<<TOIE1);
		TCCR1B = (1<<CS12)|(0<<CS11)|(1<<CS10); 
		eeprom_write_byte(&var_2, 1); //0
	}
	if (i==2) //fast
	{
		TIMSK |= (1<<TOIE1);
		TCCR1B = (0<<CS12)|(1<<CS11)|(0<<CS10); 
		eeprom_write_byte(&var_2, 2); //0
	}
	if (i==3) //always on
	{
		TIMSK |= (1<<TOIE1);
		TCCR1B = (0<<CS12)|(0<<CS11)|(1<<CS10); 
		eeprom_write_byte(&var_2, 3); //0
	}	 
}