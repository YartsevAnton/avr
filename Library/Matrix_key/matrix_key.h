/*** 	in main declare ports:

	DDRB |= (1 << PB3)|(1 << PB2)|(1 << PB1)|(1 << PB0);  
	DDRB &= ~(1 << PB7)|(1 << PB6)|(1 << PB5)|(1 << PB4);
	PORTB = 0xF0;

	in while:
	
	while(1)
	{
	if(scan_key()==1){
		PORTD = (1<<PD5);
	}

	if(scan_key()==2)
	{
		//PORTD = 0xF0;;
	}
		
	if(scan_key()==3)
	{
		//PORTD = 0xF0;
	}

	if(scan_key()==4)
	{
		//PORTD = 0xF0;
	}

	if(scan_key()==5) 
	{
		//PORTD = 0xF0;
	}

	if(scan_key()==6)
	{
		//PORTD = 0xF0;
	}
		
	if(scan_key()==7) 
	{
		//PORTD = 0xF0;
	}
		
	if(scan_key()==8) 
	{
		//PORTD = 0xF0;
	}

	if(scan_key()==9) 
	{
		//PORTD = 0xF0;
	}
		
	if(scan_key()==11) 
	{
		//PORTD = 0xF0;
	}
	}
 ***/

unsigned char key_tab[4] = {0b11111110,
                            0b11111101,
                            0b11111011,
                            0b11110111};	

unsigned char scan_key(void)
	{
		unsigned char key_value = 0; // buff (if no press button, then = 0)
		unsigned char i;
	
		for(i = 0;i < 5;i++){
			
		/*
		1 string: --1--2--3--a--
		2 string: --4--5--6--b--
		3 string: --7--8--9--c--
		4 string: --*--0--#--d--
		*/

		//--------------------------------------------------
		if(i == 0)
		{
			PORTB = 0b11111110; // install 0 in 1 string
			_delay_ms(1);
			switch (PINB & 0x70)
			{
				case 0b01100000: key_value = 1;
				return key_value;

				case 0b01010000: key_value = 2;
				return key_value;
  
				case 0b00110000: key_value = 3;
				return key_value;
			}
		}
		//--------------------------------------------------

		//--------------------------------------------------
		if(i == 1)
		{
			PORTB = 0b11111101; // install 0 in 2 string
			_delay_ms(1);
			switch (PINB & 0x70)
			{
				case 0b01100000: key_value = 4;
				return key_value;
				
				case 0b01010000: key_value = 5;
				return key_value;
  
				case 0b00110000: key_value = 6;
				return key_value;
			}
		}
		//--------------------------------------------------

		//--------------------------------------------------
		if(i == 2)
		{
			PORTB = 0b11111011; // install 0 in 3 string
			_delay_ms(1);
			switch (PINB & 0x70)
			{
				case 0b01100000: key_value = 7;
				return key_value;
				
				case 0b01010000: key_value = 8;
				return key_value;
				
				case 0b00110000: key_value = 9;
				return key_value;
			}
		}
		//--------------------------------------------------

		//--------------------------------------------------
		if(i == 3)
		{
			PORTB = 0b11110111; // install 0 in 4 string
			_delay_ms(1);
			switch (PINB & 0x70)
			{
				case 0b01100000: key_value = 10; //*			
  				return key_value;
  				
				case 0b01010000: key_value = 11; //0
				return key_value;
  
				case 0b00110000: key_value = 12; //#
				return key_value;
			}
		}

		if(i == 4){
		}
	}
	return key_value;
}
